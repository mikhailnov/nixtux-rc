#
PREFIX = /

all:
	@ echo "Nothing to compile. Use: make install, make uninstall"

install:
	install -d $(DESTDIR)/$(PREFIX)/usr/sbin
	install -m0755 ./nixtux-rc.sh $(DESTDIR)/$(PREFIX)/usr/sbin/nixtux-rc
	
	install -d $(DESTDIR)/$(PREFIX)/lib/systemd/system
	install -d $(DESTDIR)/$(PREFIX)/lib/systemd/system-preset
	install -m0644 ./nixtux-rc.service $(DESTDIR)/$(PREFIX)/lib/systemd/system/nixtux-rc.service
	install -m0644 ./nixtux-rc.timer $(DESTDIR)/$(PREFIX)/lib/systemd/system/nixtux-rc.timer
	install -m0644 ./85-nixtux-rc.preset $(DESTDIR)/$(PREFIX)/lib/systemd/system-preset/85-nixtux-rc.preset
	
	install -d $(DESTDIR)/$(PREFIX)/usr/share/nixtux-rc/keys
	install -m 0440 ./keys/mikhailnov_pub.gpg $(DESTDIR)/$(PREFIX)/usr/share/nixtux-rc/keys/mikhailnov_pub.gpg
	
uninstall:
	rm -fv $(PREFIX)/usr/sbin/nixtux-rc
	rm -fvr $(PREFIX)/usr/share/nixtux-rc/
	rm -fv $(PREFIX)/lib/systemd/system/nixtux-rc*
 
