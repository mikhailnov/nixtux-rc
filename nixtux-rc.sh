#!/bin/bash
# not implemented yet

# https://gist.github.com/earthgecko/3089509
random_string_dirname="$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)"
random_string_filename="$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)"

if [ ! -w "/tmp" ]; then
	echo "Cannot write to /tmp, cannot continue!"
	exit 1
fi

work_dir="/tmp/${random_string_dirname}"

if [ ! -z "$work_dir" ]; then rm -fvr "$work_dir"; fi
mkdir -p "$work_dir"
chown -R root:root "$work_dir"
chmod -R 0400 "$work_dir"

# сделать /etc/nixtux-rc.d/10-nixtux.conf и в него ссылки, откуда скачать, для antime можно 20-antime.conf
