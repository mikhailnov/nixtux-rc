Summary: NixTux Remote Control
Name: nixtux-rc
Version: 0.1
Release: 4
License: GPL
Group: System/Configuration/Packaging
Url: https://gitlab.com/mikhailnov/nixtux-rc.git
BuildArch: noarch
Packager: Mikhail Novosyolov <mikhailnov@dumalogiya.ru>
Source0: %name-%version.tar

BuildRequires: systemd
BuildRequires(pre): rpm-macros-fedora-compat
Requires: gnupg
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd

%description
This package contains NixTux Remote Control for corporate machines

%prep
%setup

%install
%make_install DESTDIR=%buildroot install

%post
%systemd_post %name.timer

%preun
%systemd_preun %name.timer

%postun
%systemd_postun_with_restart %name.timer

%files
/*

%changelog
* Tue Jul 27 2018 Mikhail Novosyolov <mikhailnov@dumalogiya.ru> 0.1-4
- initial build

